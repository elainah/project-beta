# CarCar

Team:
* Colin Prise, Sales
* Elaine Kwoh, Services

## Design

CarCar is the premiere solution for automobile dealership management! This web application gives the  ability to track sales, service appointments, employees and customers. It runs through three independent microservices: Inventory, Sales and Service. Sales and Service microservers are set to poll at a default 60secs for data from Inventory. The data models can be found in the models.py files inside each microservice or listed on the diagram. This program uses Django, Docker, React frontend and RESTful APIs.

Key feaures of this application:

    PostgreSQL database holds the data of all of the microservices

    Service microserver handles service appointment times, technicians, vehicle owner and service records.

    Inventory microserver manages vehicle information with VIN as the unique identifier that is used by the Service and Sales microservers.

    Sales microserver

Diagram can be found in the carcar.png of this directory or at:
https://excalidraw.com/#json=pKqgSKrq6__uxehXgOGpT,ehISk7odghhprHfBpfmjmw

INSTALLATION:  

FORK and CLONE the repository at: https://gitlab.com/elainah/project-beta.git  

    (git clone <insert HTTPS>)
With Docker running and use the following commands:  

    1.docker volume create beta-data
    2.docker-compose build
    3.docker-compose up
There should be a total of 7 containers running.  

The React Front end can be accessed at http://localhost:3000.  

Service API - http://localhost:8080  

Sales API - http://localhost:8090  

Inventory API - http://localhost:8100  

## Inventory Microservice

MANUFACTURERS  

List manufacturers  

- GET	    http://localhost:8100/api/manufacturers/  

Create a manufacturer	          

- POST	http://localhost:8100/api/manufacturers/  

Get a specific manufacturer	    

- GET	    http://localhost:8100/api/manufacturers/:id/  

Update a specific manufacturer	

- PUT	    http://localhost:8100/api/manufacturers/:id/  

Delete a specific manufacturer	  

- DELETE	http://localhost:8100/api/manufacturers/:id/  

 
<details><summary>Creating and updating a manufacturer requires only the manufacturer's name.(Click to expand) </summary>
{
  "name": "Chrysler"
}
</details>
<details><summary>The return value of creating, getting, and updating a single manufacturer is its name, href, and id. (Click to expand) </summary>
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}</details>

<details><summary>The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers. (Click to expand)  </summary>
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}</details>

VEHICLE MODELS  

List vehicle models	            

- GET	    http://localhost:8100/api/models/  

Create a vehicle model	        

- POST	http://localhost:8100/api/models/  

Get a specific vehicle model	

- GET	    http://localhost:8100/api/models/:id/  

Update a specific vehicle model	

- PUT	    http://localhost:8100/api/models/:id/  

Delete a specific vehicle model	
- DELETE	http://localhost:8100/api/models/:id/

<details><summary>Creating and updating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer. (Click to expand)</summary>
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}</details>

<details><summary>Updating a vehicle model can take the name and/or the picture URL.  (Click to expand)</summary>
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}</details>

<details><summary>Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.  (Click to expand)</summary>
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}</details>

<details><summary>Getting a list of vehicle models returns a list of the detail information with the key "models".  (Click to expand)</summary>
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}</details>


AUTOMOBILE INFORMATION  

List automobiles  

- GET	    http://localhost:8100/api/automobiles/  

Create an automobile	          

- POST	http://localhost:8100/api/automobiles/  

Get a specific automobile	      

- GET	    http://localhost:8100/api/automobiles/:vin/  

Update a specific automobile	  

- PUT	    http://localhost:8100/api/automobiles/:vin/  

Delete a specific automobile	  
- DELETE	http://localhost:8100/api/automobiles/:vin/  

<details><summary>You can create an automobile with its color, year, VIN, and the id of the vehicle model.  **The unique identifier in this API uses Vehicle Identification Number (VIN) for each specific automobile not the ID.  (Click to expand)</summary>



{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}  </details>
<details><summary>As noted, you query an automobile by its VIN. For example, you would use the URL  

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/  

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer. (Click to expand)</summary>




{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",  
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}  </details>

<details><summary>You can update the color and/or year of an automobile.(Click to expand)</summary>



{
  "color": "red",
  "year": 2012
}  </details>  
<details><summary>Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information. (Click to expand)</summary>
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}</details>

## Service Microservice

Service API - http://localhost:8080

Appointments can be viewed by vehicle owner, technician or vin. Appointments can be added, created and deleted on the React frontend, through admin or a Rest Client such as Insomnia. VIP customers are identified when the VIN matches a vehicle from the dealership's inventory throught the AutomobileVO model and poller.

In this microservice you have the ability to:  

Create and list technicians - CreateTechnician.js  

Enter service appointment - CreateServiceAppointment.js  

List service appointments - ListServiceAppointments.js  

List service history by VIN - ServiceHistory.js

RESTful API  

SERVICE APPOINTMENTS  

List service appointments       

- GET	    http://localhost:8080/service/list/  

Create an appointment 	          

- POST	http://localhost:8080/service/list/  

Get a specific appointement         

- GET	    http://localhost:8080/service/list/:id/  

Update a specific appointment       

- PUT	    http://localhost:8080/service/list/:id/  

Delete a specific appointment       

- DELETE	http://localhost:8080/service/list/:id/

<details><summary>Creating a service appointment requires the Vehicle Owner, date/time of the service appointment, reason for visit and the VIN.  (Click to expand)</summary>
{
  "vehicle_owner": "test vehicle owner",
  "vin_service": "VIN 17 digits",
  "datetime": "2023-03-08 08:00",
  "reason": "oil change",
  "is_complete": false,
  "technician":"1"
}  </details>


<details><summary>The list of service appointments is a dictionary with the key "service_appointments" set to a list of service_appointments with the technician's info nested inside another dictionary.  (Click to expand)</summary>



{
	"service_appointments": [
		{
			"id": 6,
			"vehicle_owner": "test vehicle owner",
			"vin_service": "VIN 17 digits",
			"datetime": "2023-03-08T08:00:00+00:00",
			"reason": "oil change",
			"is_complete": false,
			"is_vip": false,
			"technician": {
				"id": 1,
				"technician_name": "Carl Green",
				"employee_id": "cg"
			}
		}
  ]
}  </details>

TECHNICIANS  

List technicians                  
- GET     http://localhost:8080/service/technician/  

Create technicans                 
- PUT     http://localhost:8080/service/technician/

<details><summary>Creating a technician requires a name and a unique employee id input.  (Click to expand)</summary>



{
	"technician_name": "Tommy testing",
	"employee_id": "TT"
}

The list of technicians is a dictionary with the key "technician" set to a list of technicians.
{
  "technicians": [
		{
			"id": 1,
			"technician_name": "Carl Green",
			"employee_id": "cg"
    }
  ]
}</details>

## Sales Microservice
There are four models in the Sales Microservice:  

AutomobileVO: This model creates a value object with the unique identifying property "vin." This pulls the values of automobile from the inventory.  

SalesPerson: This model creates a sales person with the properties: name and employee_number  

SalesCustomer: This model creates a sales customer with the properties: name, adress, and phone number.  

Sale: this model creates a sale record using the models: AutomobileVO, SalesPerson, SalesCustomer and also has the property price.  

To use the Sales app, you need to create an instance of automobile. Then you will need to create a sales customer, and a salesperson, then you can create a record of sale. The record of sale is viewable as a list.  

To create a salesperson:  

- POST to http://localhost:8090/api/salesperson/  
<details><summary>Click to expand</summary>


{
"name": "John",
"employee_number": 1111111
}  

response:  

{
"name": "John",
"employee_number": 1111111
}  </details>

To view a list of salespersons:  


- GET to http://localhost:8090/api/salesperson/
<details><summary>Click to expand</summary>


response:  
{
"salespersons": [
{
"name": "John",
"employee_number": 1111111
},
{
"name": "Hank",
"employee_number": 35524560
}
]
} </details>
To create a customer:
POST to http://localhost:8090/api/salescustomer/
<details><summary>Click to expand</summary>


{
"name": "Alyssa Jackson",
"address": "200 elm street",
"phone": 4252734854

}
response:
{
"name": "Alyssa Jackson",
"address": "200 elm street",
"phone": 4252734854
}
To view a list of customers:
GET to http://localhost:8090/api/salescustomer/

}  </details>

To view a list of customers:  

- GET to http://localhost:8090/api/salescustomer/  
<details><summary>Click to expand</summary>
{
"customers": [
{
"name": "Colin Prize",
"address": "1800 fake street",
"phone": "4252525222"
},
{
"name": "Alyssa Jackson",
"address": "200 elm street",
"phone": "4252734854"
},
{
"name": "Samantha Brown",
"address": "111 ne june st",
"phone": "4254054450"
},
{
"name": "Johnny Montana",
"address": "115 reno st",
"phone": "4258734560"
},
{
"name": "Timmy Walker",
"address": "255 walking st",
"phone": "4257776351"
}
]
}  </details>

To make a sale record:  

- POST to http://localhost:8090/api/sale/  
<details><summary>Click to expand</summary>
{
"price": 27000,
"vin": "2FMDK4KC4CBA27842",
"salesperson": "Hank",
"salescustomer": "Johnny Montana"
}
}  </details>

