import json
from .models import AutomobileVO, SalesCustomer, Sale, SalesPerson
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods



class AutoVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "import_href",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number"
    ]


class SalesCustomerEncoder(ModelEncoder):
    model = SalesCustomer
    properties = [
        "name",
        "address",
        "phone"
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobileVO",
        "sales_person",
        "sales_customer"
        ]
    encoders = {
        "automobileVO": AutoVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "sales_customer": SalesCustomerEncoder()
        }


@require_http_methods(["GET", "POST"])
def list_salesperson(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Error"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customers = SalesCustomer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=SalesCustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        customer = SalesCustomer.objects.create(**content)
        return JsonResponse(
                customer,
                encoder=SalesCustomerEncoder,
                safe=False
            )


@require_http_methods(["GET", "POST"])
def list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        auto_vin = content["automobile"]
        employee = content["salesperson"]
        customer= content["salescustomer"]

        try:
            automobile = AutomobileVO.objects.get(vin=auto_vin)
            if automobile.sold == False:
                content["automobile"] = automobile

            else:
                response = JsonResponse({" Message": "This vehicle is not available"})
                response.status_code = 404
                return response

        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"Message": "This vehicle does not exist"})


        try:
            salesperson = SalesPerson.objects.get(employee_number=employee)
            content["salesperson"] = salesperson

        except SalesPerson.DoesNotExist:
            response = JsonResponse({"Message": "No salesperson found"})
            response.status_code = 404
            return response

        try:
            customer = SalesCustomer.objects.get(customer_id=customer)
            content["salescustomer"] = customer

        except SalesCustomer.DoesNotExist:
            response = JsonResponse({"Message": "No customer found"})
            response.status_code = 404
            return response

        sale = Sale.objects.create(**content)
        AutomobileVO.objects.filter(vin=auto_vin).update(sold=True)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )
