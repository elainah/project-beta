from django.urls import path
from .views import list_customer, list_sale, list_salesperson

urlpatterns = [
    path("salesperson/", list_salesperson, name="list_salesperson"),
    path("salescustomer/", list_customer, name="list_customer"),
    path("sale/", list_sale, name="list_sale"),
]
