from django.urls import path
from .views import api_list_service_appointment, api_service_details,api_list_technician, api_service_history

urlpatterns = [
    path("list/", api_list_service_appointment, name="api_service_appointment"),
    path("list/<int:pk>/", api_service_details, name="api_service_details"),
    path("technician/", api_list_technician, name="api_list_technician"),
    path("list/vip/", api_service_history, name="api_service_history")
]
