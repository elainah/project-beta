from common.json import ModelEncoder
from .models import AutomobileVO, ServiceAppointment, Technician
from django.urls import reverse

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "technician_name",
        "employee_id",
    ]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vehicle_owner",
        "vin_service",
        "datetime",
        "reason",
        "is_complete",
        "is_vip",
        "technician",
        #"automobile",

    ]

    encoders = {
        "technician": TechnicianEncoder(),
        #"automobile": AutomobileVOEncoder(),
    }

class ServiceHistoryEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vehicle_owner",
        "vin_service",
        "datetime",
        "reason",
        "is_vip",
        "technician",
        "automobile",
        "service"

    ]

    encoders = {
        "technician": TechnicianEncoder(),
        "automobile": AutomobileVOEncoder(),
        "service": ServiceAppointmentEncoder(),
    }
