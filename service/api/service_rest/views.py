from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

from .encoders import (TechnicianEncoder, ServiceAppointmentEncoder)

from .models import Technician, AutomobileVO, ServiceAppointment

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_service_appointment(request):
    if request.method == "GET":
        service_appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"service_appointments": service_appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:

        content = json.loads(request.body)

        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Not a valid employee_id"},
                status=400,
            )
        service_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_details(request, pk):

    if request.method == "GET":
        try:
            service = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"Error": "Service Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service = ServiceAppointment.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"Delete error": "Service Appointment does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            service = ServiceAppointment.objects.get(id=pk)
            ServiceAppointment.objects.filter(id=pk).update(**content)

            return JsonResponse(
                service,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )

        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"Update error": "Service Appointment does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"Error": "No Technician exists"})
            response.status_code=400


    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"Error"})
            response.status_code=400


@require_http_methods(["GET", "POST"])
def api_service_history(request):
    if request.method == "GET":
        content = json.loads(request.body)
        if AutomobileVO.objects.filter(vin=content["vin"]).exists():
            content["vip"] = True
        else:
            content["vip"] = False

        service_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
