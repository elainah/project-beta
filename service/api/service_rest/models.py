from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    color = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    year = models. PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    technician_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.technician_name

class ServiceAppointment(models.Model):
    vehicle_owner = models.CharField(max_length=100)
    datetime = models.DateTimeField()
    reason = models.TextField()
    vin_service = models.CharField(max_length=17)
    is_complete = models.BooleanField(default=False)
    is_vip = models.BooleanField(null=True, blank=True, default=False)

    def __str__(self):
        return self.vehicle_owner

    def completed(self, is_complete):
        status = is_complete.objects.put(True)
        self.status = status
        self.save()

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT
    )

    # automobile = models.ForeignKey(
    #     AutomobileVO,
    #     related_name="automobile",
    #     on_delete=models.CASCADE
    # )
