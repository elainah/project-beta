import React from "react";
import { useEffect, useState } from "react";

const ListVehicleModel = () => {
  const [model, setModel] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8100/api/models/")
      .then((response) => response.json())
      .then((data) => {
        setModel(data.models);
      })

      .catch((e) => console.log("error: ", e));
  }, []);

  return (
    <>
      <h1>Vehicle Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Picture of vehicle</th>
          </tr>
        </thead>
        <tbody>
          {model.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.manufacturer.name}</td>
                <td>{model.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    alt="Car Picture"
                    width="150px"
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div>
        <a href="http://localhost:3000/sale/create/">
          <button type="button">Create new Sale</button>
        </a>
      </div>
    </>
  );
};

export default ListVehicleModel;
