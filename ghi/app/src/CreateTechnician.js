import React, { useState } from 'react';


function CreateTechnician() {
    const [name, setName] = useState("");
    const [id, setId] = useState("");
    const handleNameChange = (event) => {
        setName(event.target.value);
    }
    const handleIdChange = (event) => {
        setId(event.target.value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.technician_name = name;
        data.employee_id = id;

        const url = `http://localhost:8080/service/technician/`;

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setName('');
            setId('');
        }

    }



    return (
        <div className='my-5 container'>
            {/* // <div className="row"> */}
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange}
                                required name="name"
                                placeholder="Name"
                                type="text"
                                id="name"
                                className="form-control"
                                value={name}
                            />
                            <label htmlFor="Name">Technician Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleIdChange}
                                required name="id"
                                placeholder="Employee ID"
                                type="text"
                                id="id"
                                className="form-control"
                                value={id}
                            />
                            <label htmlFor="id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CreateTechnician;
