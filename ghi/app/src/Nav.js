import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-success">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">CarCar</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="service/appointments/list">Service Appointments List</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="service/technician/create">Create a Technician</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="service/appointments/history">Service Appointment History</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="service/appointments/create">Create Service Appointment</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="inventory/manufacturer/create">Add Manufacturer</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="inventory/vehiclemodel/create">Add Vehicle Model</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="inventory/automobile/create">Add Automobile</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="vehicles">Vehicles List</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="manufacturers">Manufacturers List</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="automobiles">Automobiles List</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="sales/person/create">Create Sales Person</NavLink></li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="customer/create">Create Customer</NavLink></li>


                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Nav;
