import { useEffect, useState } from "react";

const AutomobileForm = () => {
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [models, setModels] = useState([]);
    const [model, setModel] = useState("");

    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    const handleYearChange = (event) => {
        setYear(event.target.value);
    }
    const handleModelChange = (event) => {
        setModel(event.target.value);
    }
    const handleVinChange = (event) => {
        setVin(event.target.value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            setColor("");
            setYear("");
            setVin("");
            setModel("");

        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Create an Automobile</h1>
                    <form id="add-automobile-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange}
                                required name="color"
                                placeholder="color"
                                type="text"
                                id="color"
                                className="form-control"
                                value={color}
                            />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange}
                                required name="year"
                                placeholder="year"
                                type="number"
                                id="year"
                                className="form-control"
                                value={year}
                            />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange}
                                required name="vin"
                                placeholder="vin"
                                type="text"
                                id="vin"
                                className="form-control"
                                value={vin}
                                maxLength={17}
                            />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleModelChange}
                                required name="Model"
                                id="model"
                                className="form-select"
                                value={model} >
                                <option value="">Model</option>
                                {models.map((model) => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default AutomobileForm;
