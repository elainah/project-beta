import React from "react";

class CreateSalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      employee_number: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeEmployeeNumber =
      this.handleChangeEmployeeNumber.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const stateData = { ...this.state };
    const Url = "http://localhost:8090/api/salesperson/";
    const FetchConfig = {
      method: "POST",
      body: JSON.stringify({ ...stateData }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(Url, FetchConfig);
    if (response.ok) {
      const newSalesPerson = await response.json();
      console.log(newSalesPerson);
      const empty = {
        name: "",
        employee_number: "",
      };
      this.setState(empty);
    }
  }

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleChangeEmployeeNumber(event) {
    const value = event.target.value;
    this.setState({ employee_number: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Sales Person</h1>
            <form onSubmit={this.handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input
                  value={this.state.name}
                  onChange={this.handleChangeName}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.employee_number}
                  onChange={this.handleChangeEmployeeNumber}
                  placeholder="Employee ID Number"
                  required
                  type="text"
                  name="employee_id"
                  id="employee_id"
                  className="form-control"
                />
                <label htmlFor="employee_id">Employee ID Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default CreateSalesPersonForm;
