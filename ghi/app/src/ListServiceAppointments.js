import React, { useEffect, useState } from 'react';

function ListServiceAppointments() {
    const [serviceAppointment, setServiceAppointment] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/service/list/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setServiceAppointment(data.service_appointments);
        }
    }

    useEffect(() => { fetchData(); }, [])


    async function deleteService(service) {
        const deleteUrl = `http://localhost:8080/service/list/${service.id}/`
        const response = await fetch(deleteUrl, { method: "delete" })
        if (response.ok) {
            fetchData()

        }

    }

    async function completeService(service) {
        const completeUrl = `http://localhost:8080/service/list/${service.id}/`
        const response = await fetch(completeUrl, {
            method: "put", body: JSON.stringify({ "is_complete": true }), headers: {
                "Content-Type": "application/json"
            }
        }
        )

        if (response.ok) {
            fetchData()
        }

    }




    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Vip Status</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Action</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {serviceAppointment.filter(appointments => {
                    return (appointments.is_complete == false)
                }).map(service => {
                    return (
                        <tr key={service.id}>
                            <td>{service.vin_service}</td>
                            <td>{service.vehicle_owner}</td>
                            <td>{service.is_vip ? "yes" : "no"}</td>
                            <td>{new Date(service.datetime).toLocaleDateString()}</td>
                            <td>{new Date(service.datetime).toLocaleTimeString()}</td>
                            <td>{service.technician.technician_name}</td>
                            <td>{service.reason}</td>
                            <td>
                                <button type="button" className="btn btn-success btn-sm" onClick={() => completeService(service)}>Complete</button>
                            </td>

                            <td><button onClick={() => deleteService(service)} type="button" className="btn btn-danger btn-sm">Cancel</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ListServiceAppointments;
