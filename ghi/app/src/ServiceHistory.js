import React, { useEffect, useState } from 'react'

function ServiceHistory() {

    const [serviceAppointment, setServiceAppointment] = useState([]);
    const [searchVin, setSearchVin] = useState("");
    const [filteredAppointments, setFilteredAppointments] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/service/list/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setServiceAppointment(data.service_appointments)
        }
    }

    useEffect(() => { fetchData(); }, [])

    const handleSearchVinChange = (event) => {
        setSearchVin(event.target.value);
    }
    const handleButtonChange = (event) => {
        let result = serviceAppointment.filter(appointment => {
            return (searchVin == appointment.vin_service)
        });
        setFilteredAppointments(result);

    }


    return (
        <div>
            <div className="shadow p-4 mt-4">
                <h1>Search by VIN</h1>
                <div className="form-floating mb-3">
                    <input
                        onChange={handleSearchVinChange}
                        required name="search"
                        placeholder="search by vin"
                        type="text"
                        id="search"
                        className="form-control"
                        // value={search}
                        maxLength={17}
                    />
                    <button
                        onClick={handleButtonChange}
                        type="button"
                        className="btn btn-outline-secondary">
                        Search by VIN
                    </button>

                </div>

            </div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <td>VIN</td>
                        <td>Customer Name</td>
                        <td>Date</td>
                        <td>Time</td>
                        <td>Technician</td>
                        <td>Reason</td>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map(service => {
                        return (
                            <tr key={service.id}>
                                <td>{service.vin_service}</td>
                                <td>{service.vehicle_owner}</td>
                                <td>{new Date(service.datetime).toLocaleDateString()}</td>
                                <td>{new Date(service.datetime).toLocaleTimeString()}</td>
                                <td>{service.technician.technician_name}</td>
                                <td>{service.reason}</td>

                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div >
    );
}




export default ServiceHistory;
