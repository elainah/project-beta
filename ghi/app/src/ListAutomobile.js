import React from "react";
import { useEffect, useState } from "react";

const ListAutomobile = () => {
  const [automobile, setAutomobile] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8100/api/automobiles/")
      .then((response) => response.json())
      .then((data) => {
        setAutomobile(data.autos);
      })

      .catch((e) => console.log("error: ", e));
  }, []);

  return (
    <>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>Vin</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobile.map((autos) => {
            return (
              <tr key={autos.id}>
                <td>{autos.color}</td>
                <td>{autos.year}</td>
                <td>{autos.vin}</td>
                <td>{autos.model.name}</td>
                <td>{autos.model.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default ListAutomobile;
