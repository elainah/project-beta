import React from "react";
import { useEffect, useState } from "react";

const ListManufacturer = () => {
  const [manufacturer, setManufacturer] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8100/api/manufacturers/")
      .then((response) => response.json())
      .then((data) => {
        setManufacturer(data.manufacturers);
      })

      .catch((e) => console.log("error: ", e));
  }, []);

  return (
    <>
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturer.map((manufacturer) => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default ListManufacturer;
