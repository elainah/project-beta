import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CreateSalesPersonForm from "./CreateSalesPersonForm";
import CreateCustomerForm from "./CreateCustomerForm";
import ListManufacturer from "./ListManufacturer";
import ListAutomobile from "./ListAutomobile";
import ListVehicleModel from "./ListVehicleModel";
import CreateTechnician from './CreateTechnician';
import ListServiceAppointments from './ListServiceAppointments';
import CreateServiceAppointment from './CreateServiceAppointment';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import ServiceHistory from './ServiceHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="vehicles" element={<ListVehicleModel />} />
          <Route path="manufacturers" element={<ListManufacturer />} />
          <Route path="automobiles" element={<ListAutomobile />} />
          <Route path="sales/">
            <Route path="person/">
              <Route path="create/" element={<CreateSalesPersonForm />} />
            </Route>
            <Route path="customer/">
              <Route path="create/" element={<CreateCustomerForm />} />
            </Route>
          </Route>
          <Route path="inventory/manufacturer/create" element={<ManufacturerForm />} />
          <Route path="inventory/vehiclemodel/create" element={<VehicleModelForm />} />
          <Route path="inventory/automobile/create" element={<AutomobileForm />} />
          <Route path="service/appointments/history" element={<ServiceHistory />} />
          <Route path="service/appointments/create" element={<CreateServiceAppointment />} />
          <Route path="service/technician/create" element={<CreateTechnician />} />
          <Route path="service/appointments/list" element={<ListServiceAppointments serviceAppointments={props.serviceAppointment} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
