import { useEffect, useState } from "react";

const VehicleModelForm = () => {
    const [name, setName] = useState("");
    const [pic, setPic] = useState("");
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState("");

    const handleNameChange = (event) => {
        setName(event.target.value);
    }
    const handlePicChange = (event) => {
        setPic(event.target.value);
    }
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }


    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.picture_url = pic;
        data.manufacturer_id = manufacturer;

        const url = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setName("");
            setPic("");
            setManufacturer("");

        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Create a Vehicle Model</h1>
                    <form id="add-automobile-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange}
                                required name="name"
                                placeholder="name"
                                type="text"
                                id="name"
                                className="form-control"
                                value={name}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePicChange}
                                required name="pic"
                                placeholder="http://www.example.com"
                                type="pic"
                                id="pic"
                                className="form-control"
                                value={pic}
                            />
                            <label htmlFor="pic">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleManufacturerChange}
                                required name="manufacturer"
                                id="manufacturer"
                                className="form-select"
                                value={manufacturer} >
                                <option value="">Manufacturer</option>
                                {manufacturers.map((manufacturer) => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default VehicleModelForm;
