import React, { useEffect, useState } from 'react';

function CreateServiceAppointment() {
    const [owner, setOwner] = useState("");
    const [reason, setReason] = useState("");
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState("");
    const [datetime, setDateTime] = useState("");

    const handleOwnerChange = (event) => {
        setOwner(event.target.value);
    }
    const handleDateTimeChange = (event) => {
        setDateTime(event.target.value);
    }
    const handleReasonChange = (event) => {
        setReason(event.target.value);
    }
    const handleTechnicianChange = (event) => {
        setTechnician(event.target.value);
    }
    const handleVinChange = (event) => {
        setVin(event.target.value);
    }

    const fetchData = async () => {
        const techniciansUrl = 'http://localhost:8080/service/technician/'
        const response = await fetch(techniciansUrl)

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }


    useEffect(() => { fetchData(); }, []);




    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vehicle_owner = owner;
        data.datetime = datetime;
        data.vin_service = vin;
        data.reason = reason;
        data.technician = technician;


        const url = `http://localhost:8080/service/list/`;

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const appointment = await response.json();
            setOwner('');
            setDateTime('');
            setVin('');
            setTechnician('');
            setReason('');
        }

    }


    return (
        <div className='my-5 container'>
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleOwnerChange}
                                required name="owner"
                                placeholder="Owner"
                                type="text"
                                id="owner"
                                className="form-control"
                                value={owner}
                            />
                            <label htmlFor="owner">Vehicle Owner</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange}
                                required name="datetime"
                                placeholder="DateTime"
                                type="datetime-local"
                                id="datetime"
                                className="form-control"
                                value={datetime}
                            />
                            <label htmlFor="date">Date and Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange}
                                required name="vin"
                                placeholder="VIN"
                                type="text"
                                id="vin"
                                className="form-control"
                                value={vin}
                                maxLength={17}
                            />
                            <label htmlFor="vin">Vehicle Vin</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <textarea onChange={handleReasonChange}
                                required name="reason"
                                placeholder="Reason"
                                rows="3"
                                id="reason"
                                className="form-control"
                                value={reason}
                            /><br></br>
                            <div className="mb-3">
                                <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                                    <option value="">Technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.technician_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CreateServiceAppointment;
