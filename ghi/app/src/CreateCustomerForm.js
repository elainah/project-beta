import React from "react";

const CreateCustomerForm = () => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const handleChangeName = (event) => {
    setName(event.target.value);
  };
  const handleChangeAddress = (event) => {
    setAddress(event.target.value);
  };
  const handleChangePhone = (event) => {
    setPhone(event.target.value);

    const fetchData = async () => {
      const url = "";
    };

    useEffect(() => {
      fetchData();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.name = name;
      data.address = address;
      data.phone = phone;

      const Url = "http://localhost:8090/api/salescustomer/";
      const FetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(Url, FetchConfig);
      if (response.ok) {
        setName("");
        setAddress("");
        setPhone("");
      }
    };
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={handleChangeName}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
              </div>
              <div className="form-floating mb-3">
                <input
                  value={address}
                  onChange={handleChangeAddress}
                  placeholder="Address"
                  required
                  type="text"
                  name="address"
                  id="address"
                  className="form-control"
                />
              </div>
              <div className="form-floating mb-3">
                <input
                  value={phone}
                  onChange={handleChangePhone}
                  placeholder="Phone Number"
                  required
                  type="text"
                  name="phone"
                  id="phone"
                  className="form-control"
                />
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };
};

export default CreateCustomerForm;

